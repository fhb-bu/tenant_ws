# Generic Websocket

Websocket Generic is an Extention for the Framework Django to use provide an generic Interface

## Features
  - Full Integration of an expandable Framework for Websockets based
    on Django-Channels
  - Full Support for Tenants with own Admininterface
  - Automated Setup for Tenants and Defaultuser, you also can do this
    manually, but its harder and some migrations tasks can fail
  - Implementation for Celery Tasks

## Requirements
  - A running [DJANGO-TENANT-SCHEMES](https://github.com/bernardopires/django-tenant-schemas) Project.
  - A [redis server](https://redis.io) 


## Installation
First of all you must download the Sourcecode with git

```sh
git clone git@bitbucket.org:fhb-bu/tenant_ws_logger.git
```

For Installing necessary Python Pakages us the requirements.txt

```sh
pip install -r requirements.txt
```
its recommendable to setup a vitual environment to seperate this project
from the normal python installation, but it is not necessary

```sh
virtualenv -p python3 envname
cd envname/bin
source activate
```


### The Interface

To add individuell Websockettasks you need to inherit from the Class wsSkeleton in the File wsSkeleton.py.

## module

every Task is devided into a command and a payload. This can be defined with the class module. The cmd (command) is an string and the payload is a List of strings

## wsGroup
group represented groups of interests in Websockets. They have a name and can bind to users and also can user can added to the group at autostart

# path
all Groups of Task have an URL, so all Messages are routed separated

# incoming / outgoing
this are variables in the template with representet a list of modules. The directions are bound at the Serverside. It has influrence on the generated JS and the transport over Websockets in JASON.

# groups
this variable binds is an list and contains all groups which are binded to the the tasks

#onConnect()
all task which should do when an user is connected to the server

#onClose()
all task which should do when an user close the connection

#handleMessage()
what should the task do if an message arrives

#sendMessage()
send messages defined by the modules

## Javascipt
For all tasks the interface generates an file which named after the path and contains an hander which dispatch incomming and outgoing massages. They are very simmulare to they python derivatives

to generate the js you need to use the manage.py

```sh
python manage.py setup ws generateJS
```

## wsSettings.py

The Class WS_Parser must initializate with an list of subclases from wsTemplates. Only this  Classes will be used for the generic system.

# Celery

- set the right settingfile in the celery_app.py
- add this line to the setting.py
```sh
REDIS = {
    "host":"localhost",
    "port":"6379",
    "database":"0",
}

BROKER_URL = "redis://" + REDIS.get("host") + ":" + REDIS.get("port") + "/" + REDIS.get("database")

CHANNEL_LAYERS = {
   "default": {
       "BACKEND": "asgi_redis.RedisChannelLayer",
       "CONFIG": {
           "hosts":[BROKER_URL],
       },
       "ROUTING": "tenant_ws.routing.channel_routing",
   },
}
```

for using the right settings for the taskmanager you need to add this too:

```sh
TASKALIAS = {}
```

a taskdefintion can looks like this

```sh
TASKALIAS = {
    "SYSTEMSTATE":{
        "task":"tenant_ws_systemstate.task.tasks.SystemStatsTask",
        "cmd":"state",
        "type":"shared",
        "rate": 10,
        "period": "seconds"
    },
    "SENSORSTATE": {
        "task": "tenant_ws_systemstate.task.tasks.SensorTask",
        "cmd": "temperature",
        "type": "tenant",
        "rate": 10,
        "period": "seconds"
    },
    "PROCESSLIST": {
        "task": "tenant_ws_systemstate.task.tasks.ProcessTask",
        "cmd": "processes",
        "type": "user",
        "rate": 10,
        "period": "seconds"
    }
}
```

To add periodic Websockettasks you need to inherit from the Class WebsocketTask in the File ws_task.py.

You only need to overwrite the methode work and let the output to be dictonary.
The dictonary must declareted in a wsSkeleton to work.

to start the celery worker you musst use the following command:

```sh
python worker -A tenant_ws.celery.celery_app -B
```

to manage this task you need to implement the TaskManager from taskmanager.py in your Project


