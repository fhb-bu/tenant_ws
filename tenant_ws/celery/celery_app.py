import os
from tenant_schemas_celery.app import CeleryApp

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tenant_ws_logger.settings')
app = CeleryApp()
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(packages=["tenant_ws_systemstate.task"])
