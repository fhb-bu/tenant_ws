from celery.task import Task
from django.db import connection
from tenant_ws.wsUtils import WebsocketUtility
from tenant_ws.wsSettings import WS_PARSER


class WebsocketTask(Task):
    abstract = True
    def run(self,*args, **kwargs):
        tenant = kwargs.get("tenant")
        user = kwargs.get("user")
        path = kwargs.get("path")
        cmd = kwargs.get("cmd")
        group = kwargs.get("group")
        params = kwargs.get("params")
        old_con = connection.schema_name
        WebsocketUtility.setConnection(tenant)
        self.send(tenant,user,path,cmd,group,
                  self.work(tenant, user, path, params))
        WebsocketUtility.setConnection(old_con)
    def work(self,tenant,user,path,params):
        return {}
    def send(self,tenant,user,path,cmd,group,message):
        WS_PARSER.get(path).sendMessage(cmd,message,group,tenant,user,celery=True)

