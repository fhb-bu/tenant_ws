from django.core.management.base import BaseCommand
from tenant_ws.wsGenerator import WsGenerator

class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            WsGenerator()
            self.stdout.write(self.style.SUCCESS("Javascript generation was success"))
        except:
            self.stdout.write(self.style.ERROR("Javascript generation had fail"))