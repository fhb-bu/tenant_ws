from logging_dashboard.wsUnit import wsLogger
from tenant_ws_systemstate.wsUnit import wsSystemState
from django.contrib.auth.signals import user_logged_in, user_logged_out


WS_PARSER = {
    "logger" : wsLogger(),
    "state" : wsSystemState(),
}

# Signals
user_logged_in.connect(WS_PARSER.get("logger").setOnlineState)
user_logged_out.connect(WS_PARSER.get("logger").setOnlineState)