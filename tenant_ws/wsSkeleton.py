import json
from channels import Group
from django.conf import settings

class wsGroups:
    def __init__(self,hasUser,hasAutoStart,isShared=False):
        self.hasUser = hasUser
        self.hasAutoStart = hasAutoStart
        self.isShared = isShared

class wsSkeleton:
    def __init__(self):
        self.path = ""
        self.incoming = {}
        self.outgoing = {}
        self.groups = {}
        self.shouldOverwriting = False

    def onConnect(self,reply_channel,info_channel):
        pass

    def onClose(self,reply_channel,info_channel):
        pass

    def sendMessage(self,cmd,message,group,tenant=None,username=None):
        Group(self.generateGroupName(group,tenant,username)).send(
            self.generateJSON(cmd,self.path,message),
            immediately=True)

    def generateGroupName(self,groupname, tenant=None, username=None):
        WSGroup = self.groups.get(groupname)
        groupName = self.path + settings.SEPERATOR + groupname
        if tenant and  (not WSGroup.isShared):
              groupName += settings.SEPERATOR + tenant
        if username and WSGroup.hasUser:
            groupName += settings.SEPERATOR + username
        return groupName

    def addGroups(self,reply_channel,info_channel):
        for groupname in self.groups:
            username = info_channel.get("user")
            if not self.groups[groupname].hasUser:
                username = None
            if self.groups[groupname].hasAutoStart:
                Group(self.generateGroupName(
                    groupname,
                    info_channel.get("tenant"),
                    username
                    )
                ).add(reply_channel)

    def delGroups(self,reply_channel,info_channel):
        for groupname in self.groups:
            Group(self.generateGroupName(
                groupname,
                info_channel.get("tenant"),
                info_channel.get("user")
            )
            ).discard(reply_channel)

    def generateJSON(self,cmd,path,data):
        def_payload = self.outgoing.get(cmd)
        if self.checkJSON(def_payload,data):
            jsonData = {
                "cmd": cmd,
                "path":path
                }
            for entry in data:
                jsonData.update({entry:data[entry]})
            return {'text':json.dumps(jsonData)}
        return None

    def checkJSON(self,def_payload,data):
        if not (len(def_payload) == len(data)):
            return False
        for rule in def_payload:
            if rule in data:
                if not (type(data[rule]) == def_payload[rule]):
                    return False
            else:
                return False
        return True

    def prepareMessage(self,message,reply_channel,info_channel):
        decodeMessage = json.loads(message)
        cmd = decodeMessage.get("cmd")
        del decodeMessage["cmd"]
        if self.checkJSON(self.incoming.get(
            cmd),
            decodeMessage
        ):
            self.handleMessage(
                cmd,
                decodeMessage,
                reply_channel,
                info_channel)
        return None

    # Overwrite this
    def handleMessage(self,cmd,message,reply_channel,info_channel):
        if cmd == "":
            pass

